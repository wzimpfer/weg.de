/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestLink;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import br.eti.kinoshita.testlinkjavaapi.model.Build;
import br.eti.kinoshita.testlinkjavaapi.model.Platform;
import br.eti.kinoshita.testlinkjavaapi.model.TestCase;
import br.eti.kinoshita.testlinkjavaapi.model.TestPlan;
import br.eti.kinoshita.testlinkjavaapi.model.TestSuite;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author waldemarzimpfer
 */
public class TLConnection {

    private URL TLURL;
    private TestLinkAPI api;
    private TestPlan testPlan;
    private Platform[] platforms;
    private TestSuite[] testSuites;

    private Integer testPlanID;

    private InputStream input;

    private DateFormat df = new SimpleDateFormat("yyyyMMdd_HH:mm");
    private Properties prop = new Properties();
    private Build newBuild;
    
    private String testPlanStr;
    private String testProjectStr;
    private static final String DEVKEY = "2a7498a4930479471a2a834f5ff0927e";
    
/**
 * Folgende Daten werden benötigt um eine Verbindung zum TestLink herzustellen:
 *  * Developer Key aus TestLink, Bezeichnung des Testplans und des Testprojektes.
 * @param devkey
 * @param testPlan
 * @param testProject 
 */
    public TLConnection(String testPlan, String testProject) {
        this.testPlanStr = testPlan;
        this.testProjectStr = testProject;        
        init();
    }

    private void init() {
        try {
            this.TLURL = new URL("http://192.168.2.26/lib/api/xmlrpc/v1/xmlrpc.php");
        } catch (MalformedURLException ex) {
            Logger.getLogger(TLConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        

        api = new TestLinkAPI(TLURL, DEVKEY);
        testPlan = api.getTestPlanByName(testPlanStr, testProjectStr);
        
        testPlanID = testPlan.getId();
        testSuites = api.getTestSuitesForTestPlan(testPlanID);
        platforms = api.getTestPlanPlatforms(testPlanID);
        newBuild = api.createBuild(testPlanID, df.format(new Date()) + "_AutoBuild", "Dieses Build wurde automatisch erstellt.");
        
    }
/**
 * Methode trägt das übergebene Ergebnis ins TestLink ein.
 * @param tcprfx  Testfall ID
 * @param testCaseVersion  TEstfall Version
 * @param notes  Notizen zur Ausführung
 * @param plattform Testdevice
 * @param exc Execution Status
 */
    public void reportResult(String tcprfx, int testCaseVersion, String notes, String plattform, ExecutionStatus exc) {
        TestCase tc = null;
        try {
            tc = api.getTestCaseByExternalId(tcprfx, testCaseVersion);
            api.reportTCResult(tc.getId(), tc.getId(), testPlanID, exc, newBuild.getId(), null, notes + df.format(new Date()), Boolean.TRUE, null, 1278, plattform, null, Boolean.TRUE);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
/**
 * Gibt einen Feld mit den im Testplan angelegtenn Platformen
 * @return plattforms[]
 */
    public Platform[] getPlatforms() {
        return platforms;
    }
}
