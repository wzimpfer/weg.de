package Helpers;

import TestLink.TLConnection;
import br.eti.kinoshita.testlinkjavaapi.model.Platform;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author waldemarzimpfer
 */
public class SearchForm {

    private static WebDriver driver;
    SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");
    WebDriverWait wait;

    public SearchForm(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "Asn_DestinationAutocomplete")
    WebElement inptReiseziel;

    @FindBy(id = "cx_AsnDurationStartDate")
    WebElement inptDateFrom;

    @FindBy(id = "cx_AsnDurationEndDate")
    WebElement inptDateTo;

    @FindBy(name = "duration")
    WebElement duration;

    @FindBy(className = "cx_Autocomplete_Group")
    WebElement autocompliteItem;

    @FindBy(id = "stars")
    WebElement selectHotelkategorie;

    @FindBy(id = "cx_AsnMealTypeSelect")
    WebElement selectMealType;

    @FindBy(css = ".cx_AsnFieldset_Simple > p > button")
    WebElement btnSearch;

    @FindBy(id = "Asn_DestinationRegions")
    WebElement selectRegion;

    public boolean isInputDestinationEditable(String reiseziel) {
        try {
            inptReiseziel.sendKeys(reiseziel);
            wait.until(ExpectedConditions.elementToBeClickable(autocompliteItem));
            inptReiseziel.sendKeys(Keys.ENTER);
            //Thread.sleep(2000);            
            if (inptReiseziel.getAttribute("value").equals(reiseziel)) {
                return true;
            } else {
                return false;
            }
        } catch (NoSuchElementException e) {
            System.out.println("Element: Iput 'Reiseziel' konnte nicht lokalisiert werden \n ");
            return false;
        }
    }

    public boolean isRegionChoosable() {
        try {
            Random rand = new Random();
            wait.until(ExpectedConditions.elementToBeClickable(selectRegion));
            Select slctRegion = new Select(selectRegion);
            List<WebElement> regionen = slctRegion.getOptions();
            int index = rand.nextInt(regionen.size());
            slctRegion.selectByIndex(index);

            if (!slctRegion.getFirstSelectedOption().getText().equals("beliebig")) {
                return true;
            } else {
                return false;
            }
        } catch (NoSuchElementException e) {
            System.out.println("Element Select: 'Regionen' konnte nicht lokalisiert werden");
            return false;
        }
    }

    public boolean isInputDateFromEditable(Date fromDate) {
        try {
            wait.until(ExpectedConditions.elementToBeClickable(inptDateTo));
            inptDateFrom.click();

            WebElement calendar = driver.findElement(By.className("cx_Calendar_Months"));
            List<WebElement> months = calendar.findElements(By.tagName("table"));
            String month = getMonth(fromDate.getMonth());
            for (int i = 0; i < months.size(); i++) {
                if (months.get(i).findElement(By.tagName("caption")).getText().contains(month)) {
                    //    System.out.println(months.get(i).findElement(By.tagName("caption")).getText());
                    List<WebElement> days = months.get(i).findElements(By.tagName("td"));
                    for (WebElement day : days) {
                        if (day.getText().equals("" + fromDate.getDate())) {
                            day.click();
                            break;
                        }
                    }
                    break;
                }
            }
            if (inptDateFrom.getAttribute("value").contains(df.format(fromDate))) {
                return true;
            } else {
                return false;
            }
        } catch (NoSuchElementException e) {
            System.out.println("Element: Iput 'Frühester Hinflug' konnte nicht lokalisiert werden \n ");
            return false;
        }
    }

    public boolean isInputDateToEditable(Date toDate) {
        try {
            wait.until(ExpectedConditions.elementToBeClickable(inptDateTo));
            inptDateTo.click();

            WebElement calendar = driver.findElement(By.className("cx_Calendar_Months"));
            List<WebElement> months = calendar.findElements(By.tagName("table"));
            String month = getMonth(toDate.getMonth());
            for (int i = 0; i < months.size(); i++) {

                if (months.get(i).findElement(By.tagName("caption")).getText().contains(month)) {
                    //     System.out.println(months.get(i).findElement(By.tagName("caption")).getText());
                    List<WebElement> days = months.get(i).findElements(By.tagName("td"));
                    for (WebElement day : days) {
                        if (day.getText().equals("" + toDate.getDate())) {
                            day.click();
                            break;
                        }
                    }
                    break;
                }
            }
            if (inptDateTo.getAttribute("value").contains(df.format(toDate))) {
                return true;
            } else {
                return false;
            }
        } catch (NoSuchElementException e) {
            System.out.println("Element: Iput 'Frühester Hinflug' konnte nicht lokalisiert werden \n ");
            return false;
        }
    }

    public boolean isDurationChoosable(String dur) {
        try {
            // duration.click();
            Select durSelect = new Select(duration);
            durSelect.selectByVisibleText(dur);
            if (durSelect.getFirstSelectedOption().getText().equals(dur)) {
                return true;
            } else {
                return false;
            }
        } catch (NoSuchElementException e) {
            System.out.println("Element: Select 'Reisedauer' konnte nicht lokalisiert werden");
            return false;
        }
    }

    public boolean isHotelkategorieChoosable(String kategorie) {
        try {

            Select hotelkategorie = new Select(selectHotelkategorie);
            hotelkategorie.selectByVisibleText(kategorie);
            if (hotelkategorie.getFirstSelectedOption().getText().equals(kategorie)) {
                return true;
            } else {
                return false;
            }
        } catch (NoSuchElementException e) {
            System.out.println("Element: Select 'Hotelkategorie' konnte nicht lokalisiert werden");
            Logger.getLogger(SearchForm.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
    }

    public boolean isMealTypeChoosable(String mealType) {
        try {

            Select hotelkategorie = new Select(selectMealType);
            hotelkategorie.selectByVisibleText(mealType);
            if (hotelkategorie.getFirstSelectedOption().getText().equals(mealType)) {
                return true;
            } else {
                return false;
            }
        } catch (NoSuchElementException e) {
            System.out.println("Element: Select 'Verpflegung' konnte nicht lokalisiert werden");
            Logger.getLogger(SearchForm.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
    }

    public boolean isSeachButtonClickable() {
        try {
            wait.until(ExpectedConditions.elementToBeClickable(btnSearch));
            btnSearch.click();
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Element: Button 'Angebote finden' konnte nicht lokalisiert werden");
            return false;
        }
    }

    public String getMonth(int month) {
        String monthstr = new DateFormatSymbols().getMonths()[month];
        return monthstr;
    }

}
