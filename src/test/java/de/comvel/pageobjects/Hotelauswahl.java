/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.comvel.pageobjects;

import java.util.List;
import java.util.Random;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author waldemarzimpfer
 */
public class Hotelauswahl {
    
    private static WebDriver driver;
    WebDriverWait wait;
    Random rand;
    
    public Hotelauswahl(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
        rand = new Random();
        PageFactory.initElements(driver, this);
    }
    
    @FindBy(className = "cx_ProductItem")
    List<WebElement> hotels;
    
    public boolean isPageHotelauswalCorrectlyLoaded(){
        try {
            
            if (!hotels.isEmpty()) {
                WebElement hotel = hotels.get(rand.nextInt(hotels.size()));                
                if (isHotelItemCorrect(hotel)) {
                    return true;
                }else{
                    return false;
                }
            }else{
                System.out.println("Hotelliste ist leer");
                return false;
            }
            
        } catch (NoSuchElementException e) {
            System.out.println("Hotelliste kann nicht lokalisiert werden.");
            return false;
        }
    }
    
    public boolean isHotelItemCorrect(WebElement element){
        try {
            WebElement itemImage = element.findElement(By.className("cx_ProductItem_Image"));
            WebElement hoteTitel = element.findElement(By.className("cx_ProductItem_Title"));
            WebElement hotelPreis = element.findElement(By.className("cx_ProductItem_PriceValue"));
            WebElement btnAngeboteFInden = element.findElement(By.className("cx_ProductItem_Booking")).findElement(By.tagName("a"));
   /*         System.out.println("Ist Image-Source leer : "+itemImage.findElement(By.tagName("img")).getAttribute("data-src").isEmpty());
            System.out.println("Hoteltitel" +hoteTitel.getText());
            System.out.println("Hoteltitel ist nicht verlinkt"+ hoteTitel.findElement(By.tagName("a")).getAttribute("href").isEmpty());
            System.out.println("Hotelpreis ist: "+hotelPreis.getText());
            System.out.println("Button mit den Text: '"+btnAngeboteFInden.getText()+ "' ist vorhanden."); */
            if ((!itemImage.findElement(By.tagName("img")).getAttribute("data-src").isEmpty()) && (!hoteTitel.getText().isEmpty()) && (!hotelPreis.getText().isEmpty())
                    && btnAngeboteFInden.getText().equals("Zum Angebot")) {
                return true;
            }else{
                return false;
            }
            
        } catch (NoSuchElementException e) {
            System.out.println("Eienr der HotelItem-Elemente ist nicht vorhanden");
            return false;
        }
    }
    
    public boolean isHotelTitelLinked(){
        try {
            WebElement element = hotels.get(rand.nextInt(hotels.size()));
            WebElement hotelTitelLink = element.findElement(By.className("cx_ProductItem_Title"));
            String hotelTitelOverviewpage = hotelTitelLink.getText();
            hotelTitelLink.findElement(By.tagName("a")).click();
            WebElement resultPageTitel = driver.findElement(By.className("cx_LocationDetails_LocationName"));
            wait.until(ExpectedConditions.elementToBeClickable(resultPageTitel));                       
            String hotelTitelDetailPage = resultPageTitel.findElement(By.tagName("h1")).getText();
           
            if (hotelTitelOverviewpage.equalsIgnoreCase(hotelTitelDetailPage)) {
                return true;
            }else{
                return false;
            }
        } catch (NoSuchElementException e) {
            System.out.println("Beim Aufruf der Hoteldetailseite ist ein Fehler aufgetretten.");
            return false;
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return false;
        }
            
    }
    
}
