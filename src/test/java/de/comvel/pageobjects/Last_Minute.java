/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.comvel.pageobjects;

import Helpers.SearchForm;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author waldemarzimpfer
 */
public class Last_Minute {

    private static WebDriver driver;
    private static SearchForm searchForm;
    private static WebDriverWait wait;
   
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    public Last_Minute(WebDriver driver) {
        //   this.client = client;
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
        //    this.tlc = tlc;
        PageFactory.initElements(driver, this);
        searchForm = new SearchForm(driver);
    }

    @FindBy(id = "cx_nav-1")
    WebElement linkLM;

    @FindBy(id = "cx_Quicksearch_CvLastMinute_Autocomplete")
    WebElement inptReiseziel;

    @FindBy(id = "cx_Quicksearch_CvLastMinute_EarliestDeparture")
    WebElement inptDateFrom;

    @FindBy(id = "cx_Quicksearch_CvLastMinute_LatestReturn")
    WebElement inptDateTo;

    @FindBy(name = "duration")
    WebElement duration;

    @FindBy(name = "travellers")
    WebElement travellers;

    public boolean isLMpageLoaded() {
        linkLM.click();

        if (driver.getTitle().equals("Last Minute Urlaub und Reisen günstig bei weg.de")) {           
            //searchForm = new SearchForm(driver);
            return true;
        } else {           
            return false;
        }
    }

    public boolean isInputDestinationEditable(String reiseziel) {
        try {
            return searchForm.isInputDestinationEditable(reiseziel);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
        
    }
    
    public boolean isRegionChoosable(){
        return searchForm.isRegionChoosable();
    }

    public boolean isInputDateFromEditable(Date fromDate) {
        return searchForm.isInputDateFromEditable(fromDate);
    }

    public boolean isInputDateToEditable(Date toDate) {
        return searchForm.isInputDateToEditable(toDate);
    }

    public boolean isDurationChoosable(String dur) {
        return searchForm.isDurationChoosable(dur);
    }

    public boolean isHotelkategorieChoosable(String kategorie) {
        return searchForm.isHotelkategorieChoosable(kategorie);
    }

    public boolean isMealTypeChoosable(String mealType) {
        return searchForm.isMealTypeChoosable(mealType);
    }

    public boolean isSeachButtonClickable() {
        return searchForm.isSeachButtonClickable();
    }

    public boolean isPageReisezieleLoaded() {
        try {
            WebElement navBarElem = driver.findElement(By.className("cx_ProgressBar_active"));
            wait.until(ExpectedConditions.elementToBeClickable(navBarElem));
            if (navBarElem.getText().contains("Reiseziele")) {
                System.out.println(navBarElem.getText());
                return true;
            }else{
                return false;
            }
        } catch (Exception e) {
            System.out.println("Element Navigationspunkt 'Reiseziel' konnte nicht lokalisiert werden.");
            return false;
        }
    }
    public boolean isPageHotelinfosLoaded() {
        try {
            WebElement navBarElem = driver.findElement(By.className("cx_ProgressBar_active"));
            wait.until(ExpectedConditions.elementToBeClickable(navBarElem));
            if (navBarElem.getText().contains("Hotelauswahl")) {                
                return true;
            }else{
                return false;
            }
        } catch (Exception e) {
            System.out.println("Element Navigationspunkt 'Reiseziel' konnte nicht lokalisiert werden.");
            return false;
        }
    }

    /*
    String getPlattformsbyName(String platform) {
        String platformName = null;
        for (int i = 0; i < platforms.length; i++) {
            if (platforms[i].getName().toLowerCase().contains(platform)) {
                platformName = platforms[i].getName();
            }
        }
        return platformName;
    }    
     */
    public boolean isTrevellersChooseble(String countTrvl) {

        return true;
    }

}
