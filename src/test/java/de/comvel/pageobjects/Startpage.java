package de.comvel.pageobjects;

import Helpers.SearchForm;
import TestLink.TLConnection;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import br.eti.kinoshita.testlinkjavaapi.model.Platform;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


/**
 *
 * @author waldemarzimpfer
 */
public class Startpage {

    private static WebDriver driver;
    
//    private static TLConnection tlc;
//    private static Platform platforms[];
    String client;
    private static final String prefix_STARSEITE = "WEG-1";
    private static final String prefix_LM_LINK = "WEG-3";
    SearchForm searchForm;
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");    
    

    public Startpage(WebDriver driver) {
        this.driver = driver;
 //       this.tlc = tlc;
  //      platforms = tlc.getPlatforms();
        this.client = client;
        PageFactory.initElements(driver, this);
    }
        
    @FindBy(id = "cx_Quicksearch_CvLastMinute_Autocomplete")
    WebElement inptReiseziel;

    @FindBy(id = "cx_Quicksearch_CvLastMinute_EarliestDeparture")
    WebElement inptDateFrom;

    @FindBy(id = "cx_Quicksearch_CvLastMinute_LatestReturn")
    WebElement inptDateTo;

    @FindBy(name = "duration")
    WebElement duration;

    @FindBy(name = "travellers")
    WebElement travellers;
    

    public boolean isStartpageLoaded() {
        driver.get("http://weg.de");

        if (driver.getTitle().equals("weg.de - Urlaub günstig buchen beim mehrfachen Testsieger")) {    
    //        tlc.reportResult(prefix_STARSEITE, 1, "Test wurde autmatisiert ausgeführt", getPlattformsbyName(client), ExecutionStatus.PASSED);
            searchForm = new SearchForm(driver);
            return true;            
        } else {      
      //      tlc.reportResult(prefix_STARSEITE, 1, "Test wurde autmatisiert ausgeführt", getPlattformsbyName(client), ExecutionStatus.FAILED);
            return false;
        }
    }

   public boolean isInputDestinationEditable(String reiseziel) {
        return searchForm.isInputDestinationEditable(reiseziel);
    }

    public boolean isInputDateFromEditable(Date fromDate) {
        return searchForm.isInputDateFromEditable(fromDate);
    }

    public boolean isInputDateToEditable(Date toDate) {
       return searchForm.isInputDateToEditable(toDate);
    }

    public boolean isDurationChooseble(String dur) {
        return searchForm.isDurationChoosable(dur);
    }

 /*   String getPlattformsbyName(String platform) {
        String platformName = null;
        for (int i = 0; i < platforms.length; i++) {
            if (platforms[i].getName().toLowerCase().contains(platform)) {
                platformName = platforms[i].getName();
            }
        }
        return platformName;
    }*/
    
    
    public boolean isTrevellersChooseble(String countTrvl) {
        
        return true;
    }

}
