/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.comvel.pageobjects;

import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author waldemarzimpfer
 */
public class Hotelinfos {
    
    private static WebDriver driver;
    WebDriverWait wait;

    public Hotelinfos(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
        PageFactory.initElements(driver, this);
    }
    
    @FindBy(className = "cx_LocationDetails_LocationName")
    WebElement hotelTitle;
    @FindBy(className = "x_Button")
    WebElement btnZuDenAngeboten;
    @FindBy(className = "cx_SelectedFeature")
    List<WebElement> choiceBoxItems;
    
    public boolean isHotelInfoPageCorrectlyLoaded(){
        try {
            
        } catch (Exception e) {
        }
        return true;
    }
}
