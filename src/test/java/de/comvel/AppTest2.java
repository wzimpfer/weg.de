package de.comvel;
import junit.framework.TestCase;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/*
@TestObject(testObjectApiKey = "46DE43F6F25E483E97A36227C4A91907", testObjectSuiteId = 3)
@RunWith(TestObjectAppiumSuite.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)*/
@RunWith(Suite.class)
@Suite.SuiteClasses({de.comvel.userstories.LastMinute_BookingFF.class})
public final class AppTest2 extends TestCase{
    
}