/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.comvel.userstories;

import TestLink.TLConnection;
import br.eti.kinoshita.testlinkjavaapi.model.Platform;
import de.comvel.pageobjects.Hotelauswahl;
import de.comvel.pageobjects.Last_Minute;
import de.comvel.pageobjects.Startpage;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import static org.junit.Assert.assertTrue;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author waldemarzimpfer
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LastMinute_BookingFF {

    static WebDriver driver;
    static Startpage startpage;
    static Last_Minute last_Minute;
    static Hotelauswahl hotelauswahl;
    //   static TLConnection tlc;
    static Date date = new Date();
    static Date fromDate;
    static Date toDate;

    @BeforeClass
    public static void setUp() throws MalformedURLException {
        System.setProperty("webdriver.gecko.driver", "FFDriver/geckodriver");
        //     System.setProperty("webdriver.chrome.driver", "Chromedriver/chromedriver");
        /* DesiredCapabilities caps = new DesiredCapabilities(DesiredCapabilities.firefox());
        ChromeOptions options = new ChromeOptions();
        options.addArguments("incognito");
        caps.setCapability(ChromeOptions.CAPABILITY, options);*/
        //  driver = new RemoteWebDriver(new URL("http://192.168.2.17:4444/wd/hub"), caps);
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        initDates();
        //    tlc = new TLConnection("Last_Minute", "WegDE");
        startpage = new Startpage(driver);
        last_Minute = new Last_Minute(driver);
        hotelauswahl = new Hotelauswahl(driver);

    }

    static void initDates() {
        fromDate = new Date(date.getYear(), date.getMonth() + 1, date.getDate() + 7);
        toDate = new Date(fromDate.getYear(), fromDate.getMonth() + 1, fromDate.getDate() + 2);
    }

    @Test
    public void a_WEG_1_Startseite_Aufruf_Aufbau() {
        try {
            assertTrue("Startseite wurde nicht geladen", startpage.isStartpageLoaded());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    @Test
    public void b_WEG_3_LastMInuteLinkTest() {

        try {
            assertTrue("Last Minute Seite wurde nicht geladen", last_Minute.isLMpageLoaded());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void c_WEG_18_Last_Minute_2_Personen() {
        assertTrue("Input 'Reiseziel' konnte nicht editiert werden", last_Minute.isInputDestinationEditable("Spanien"));

        assertTrue("Select 'Regionen' konnte nicht ausgewählt werden", last_Minute.isRegionChoosable());

        assertTrue("Input 'Frühester Hinflug' konnte nicht editiert werden", last_Minute.isInputDateFromEditable(fromDate));

        assertTrue("Input 'Spätester Rückflug' konnte nicht editiert werden", last_Minute.isInputDateToEditable(toDate));

        assertTrue("Select 'Reisedauer' konnte nicht ausgewählt werden", last_Minute.isDurationChoosable("9 - 12 Tage"));

        assertTrue("Select 'Hotelkategorie' konnte nicht ausgewählt werden", last_Minute.isHotelkategorieChoosable("mind. 3 Sterne"));

        assertTrue("Select 'Verpflegung' konnte nicht ausgewählt werden", last_Minute.isMealTypeChoosable("mind. All Inclusive"));

        assertTrue("Button 'Angebite finden' konnte nicht ausgewählt werden", last_Minute.isSeachButtonClickable());

        assertTrue("Seite 'Reiseziele' wurde nicht geladen", last_Minute.isPageHotelinfosLoaded());
    }

    @Test
    public void d_WEG_20_Suchergebnisseite() {
        assertTrue("Hotelauswahlseite wurde nicht korrekt geladen", hotelauswahl.isPageHotelauswalCorrectlyLoaded());
    }

    @Test
    public void e_WEG_22_Hoteldetails_TitelClick() {
        assertTrue("Hoteldetailseite wurde nicht korrekt geladen.", hotelauswahl.isHotelTitelLinked());
    }

    @AfterClass
    public static void tearDown() throws InterruptedException {
        System.out.println("Last Minute Test mit FireFoxe wird beendet ... ");
        Thread.sleep(3000);
        driver.quit();
    }
}
